var express = require("express");
var router = express.Router();

var AuthController = require("../controllers/Auth");
var AuthCheck = require("../middleware/Auth.Check");

// GET
router.get("/account", AuthCheck, AuthController.auth_get_all_account);
router.get("/account/:userId", AuthCheck, AuthController.auth_find_account);
// POST
router.post("/signIn",  AuthController.auth_SignIn);
router.post("/signUp", AuthController.auth_SignUp);
router.post("/confirmation", AuthController.auth_confirmation_SignUp_email);
router.post("/resend", AuthController.auth_resend_email);
router.post("/forgotPassword", AuthController.auth_forgotPassword);
router.post("/resetPassword", AuthController.auth_resetPassword);
// DELETE
router.delete("/account/:userId", AuthController.auth_DeleteUser);

module.exports = router;
var mongoose = require("mongoose");

var AuthSchema = mongoose.Schema({
  _id: mongoose.Types.ObjectId,
  email: {
    type: String,
    required: true,
    unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  password: {
    type: String,
    required: true,
  },
  verificationCode: {
    type: String
  },
  resetPasswordExpires: {
    type: Date,
    default: Date.now
  },
  isVerified: { type: Boolean, default: false },
})

module.exports = mongoose.model("Auth", AuthSchema);
var mongoose = require("mongoose");
var bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
var nodemailer = require('nodemailer');
var crypto = require('crypto');
var Auth = require("../models/Auth");

const transporter = nodemailer.createTransport({
  service: 'Sendgrid',
  auth: {
    user: process.env.SENDGRID_USERNAME,
    pass: process.env.SENDGRID_PASSWORD
  }
});
const codeDigits = Math.floor(1000 + Math.random() * 9000) // 4 Digits
// const codeDigits = Math.floor(100000 + Math.random() * 900000) // 6 Digits

//Auth get list Account
exports.auth_get_all_account = (req, res, next) => {
  Auth.find()
    .select('_id, email')
    .then(result => {
      res.status(200).send({
        status: true,
        count: result.length,
        user: result,
        message: 'Get all account Success !'
      })
    }).catch(err => {
      res.status(500).send({
        status: false,
        err,
        message: 'Get failed !'
      })
    })
}

// Find Account User
exports.auth_find_account = (req, res, next) => {
  var id = req.params.userId;
  Auth.findById(id).select("").exec().then(results => {
    if (results) {
      res.status(200).send({
        status: true,
        message: "Find by user successful!",
        results: {
          email: results.email,
          _id: results._id
        }
      })
    } else {
      res.status(404).send({
        status: false,
        message: "Find by user failed!",
        results: null
      })
    }
  }).catch(error => {
    res.status(500).send({
      status: false,
      message: "Not found!",
      error
    })
  })
}

//Auth Register
exports.auth_SignUp = (req, res, next) => {
  Auth.find({ email: req.body.email }).exec().then(user => {
    if (user.length >= 1) {
      return res.status(401).send({
        status: false,
        message: "Email exist"
      })
    } else {
      bcrypt.hash(req.body.password, 10, (error, hash) => {
        if (error) {
          return res.status(500).send({
            status: false,
            message: "Not Found!",
            error
          })
        } else {
          var user = new Auth({
            _id: new mongoose.Types.ObjectId(),
            email: req.body.email,
            password: hash,
            verificationCode: codeDigits,
            resetPasswordExpires: Date.now() + 3600000
          });
          // verify email
          user.save().then(result => {
            if (result) {
              var mailOptions = {
                from: 'duypx@htactive.com',
                to: user.email,
                subject: 'Code reset password',
                text: 'Hello,\n\n' + 'Please verify your account with this code: ' + codeDigits + '.\n'
                // text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.'Hello,\n\n' + 'Please verify your account with this code: \n\n ' + codeDigitsheaders.host + '\/auth' + '\/resetPassword\/' + token + '.\n'
              };
              transporter.sendMail(mailOptions, (err) => {
                if (err) {
                  return res.status(500).send({
                    status: false,
                    message: err.message
                  });
                } else {
                  res.status(200).send({
                    status: true,
                    message: 'register account successful! A verification email has been send to ' + user.email + '.'
                  });
                }
              });
            } else {
              res.status(401).send({
                status: false,
                message: 'send code authentication failed'
              })
            }
          }).catch(error => {
            res.status(500).send({
              status: false,
              message: "not found, register failed!",
              error
            })
          })
        }
      })
    }
  }).catch(error => {
    res.status(500).send({
      status: false,
      message: "register failed",
      error
    })
  })
}

//Auth confirmation
exports.auth_confirmation_SignUp_email = (req, res, next) => {
  Auth.findOne({
    email: req.body.email
  }).exec().then(user => {
    if (user.isVerified) {
      return res.status(401).send({
        status: false,
        message: 'This account has already been verified. Please log in.'
      });
    }
    if (user.verificationCode !== req.body.verificationCode) {
      return res.status(400).send({
        status: false,
        message: 'The verification code is not valid.'
      });
    }
    user.isVerified = true;
    user.verificationCode = null;
    user.resetPasswordExpires = null;
    user.save(err => {
      if (err) {
        return res.status(500).send({
          status: false,
          message: err.message
        });
      } else {
        res.status(200).send({
          status: true,
          message: "The account has been verified. Please log in."
        });
      }
    });
  }).catch(error => {
    res.status(500).send({
      status: true,
      message: "The account has been verified. Please log in.",
      error
    });
  })
}

// Auth Resend
exports.auth_resend_email = (req, res, next) => {
  Auth.findOne({ email: req.body.email }).exec().then(user => {
    if (!user) {
      return res.status(400).send({
        status: false,
        message: 'We were unable to find a user with that email.',
      });
    }
    if (user.isVerified) {
      return res.status(400).send({
        status: false,
        message: 'This account has already been verified. Please log in.'
      });
    }
    var code_resend_email = Math.floor(1000 + Math.random() * 9000);
    user.update({
      isVerified: false,
      verificationCode: code_resend_email,
      resetPasswordExpires: Date.now() + 3600000
    }).then(result => {
      if (result) {
        var mailOptions = {
          from: 'duypx@htactive.com',
          to: user.email,
          subject: 'Code reset password',
          text: 'Hello,\n\n' + 'Please verify your account with this code: ' + code_resend_email + '.\n'
          // text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.'Hello,\n\n' + 'Please verify your account with this code: \n\n ' + codeDigitsheaders.host + '\/auth' + '\/resetPassword\/' + token + '.\n'
        };
        transporter.sendMail(mailOptions, (err) => {
          if (err) {
            return res.status(500).send({
              status: false,
              message: err.message
            });
          } else {
            res.status(200).send({
              status: true,
              message: 'A verification email has been sent to ' + user.email + '.'
            });
          }
        });
      } else {
        res.status(401).send({
          status: false,
          message: 'Send mail forgot password failed!'
        })
      }
    })
  }).catch(error => {
    return res.status(500).send({
      status: false,
      message: 'This account has already been verified. Please log in.',
      error
    });
  })
}

// Auth SignIn
exports.auth_SignIn = (req, res, next) => {
  Auth.findOne({ email: req.body.email }).exec().then(user => {
    if (user.isVerified === true) {
      if (user.length < 1) {
        return res.status(401).send({
          status: false,
          message: "Auth failed"
        })
      }
      bcrypt.compare(req.body.password, user.password, (error, results) => {
        if (error) {
          return res.status(401).send({
            status: false,
            message: "Auth failed"
          })
        } else if (results) {
          var token = jwt.sign(
            {
              email: user.email,
              userId: user._id
            },
            process.env.JWT_KEY,
            {
              expiresIn: "168h"
            }
          );
          return res.status(200).send({
            status: true,
            message: "Login Successful!",
            token
          });
        }
        res.status(400).send({
          status: false,
          message: "Auth failed"
        })
      })
    } else {
      return res.status(400).send({
        status: false,
        message: "The account has not been verified"
      })
    }
  }).catch(error => {
    res.status(500).send({
      status: false,
      message: "Login failed",
      error
    })
  })
}

// Auth Remove User
exports.auth_DeleteUser = (req, res, next) => {
  Auth.findByIdAndRemove({ _id: req.params.userId }).exec().then(user => {
    if (!user) {
      return res.status(404).send({
        message: "Note not found with id " + req.params.userId
      });
    } else {
      res.status(200).send({
        status: true,
        user,
        message: "Delete user successful!"
      })
    }
  }).catch(error => {
    res.status(500).send({
      status: false,
      error,
      message: "Delete user failed!"
    })
  })
}

// Auth forgotPassword
exports.auth_forgotPassword = (req, res, next) => {
  if (req.body.email === "") {
    res.status(401).send({
      status: false,
      message: 'Email is required'
    })
  }
  Auth.findOne({ email: req.body.email }).exec().then(user => {
    // var token = crypto.randomBytes(16).toString('hex');
    user.update({
      verificationCode: codeDigits,
      resetPasswordExpires: Date.now() + 3600000
    }).then(result => {
      if (result) {
        var mailOptions = {
          from: 'duypx@htactive.com',
          to: user.email,
          subject: 'Code reset password',
          text: 'Hello,\n\n' + 'Please verify your account with this code: ' + codeDigits + '.\n'
          // text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.'Hello,\n\n' + 'Please verify your account with this code: \n\n ' + codeDigitsheaders.host + '\/auth' + '\/resetPassword\/' + token + '.\n'
        };
        transporter.sendMail(mailOptions, (err) => {
          if (err) {
            return res.status(500).send({
              status: false,
              message: err.message
            });
          } else {
            res.status(200).send({
              status: true,
              message: 'A verification email has been sent to ' + user.email + '.'
            });
          }
        });
      } else {
        res.status(401).send({
          status: false,
          message: 'Send mail forgot password failed!'
        })
      }
    })
  }).catch(error => {
    res.status(500).send({
      status: false,
      message: "send mail forgot password failed!",
      error
    })
  })
}

// Auth reset
exports.auth_resetPassword = (req, res, next) => {
  Auth.findOne({
    verificationCode: req.body.verificationCode,
    resetPasswordExpires: {
      $gt: Date.now()
    }
  }).exec().then(user => {
    if (user) {
      if (req.body.newPassword === req.body.verifyPassword) {
        user.password = bcrypt.hashSync(req.body.newPassword, 10);
        user.verificationCode = null;
        user.resetPasswordExpires = null;
        user.save((err) => {
          if (err) {
            return res.status(422).send({
              message: err
            });
          } else {
            var mailOptions = {
              to: user.email,
              from: process.env.SENDGRID_USERNAME,
              template: 'reset-password-email',
              subject: 'Password Reset Confirmation'
            };

            transporter.sendMail(mailOptions, (err) => {
              if (err) {
                return res.status(500).send({
                  status: false,
                  message: err.message
                });
              }
              res.status(200).send({
                status: true,
                message: 'password reset successful!'
              });
            });
          }
        });
      } else {
        res.status(401).send({
          status: false,
          message: "Update new password failed!"
        })
      }
    } else {
      res.status(500).send({
        status: false,
        message: "The verification code is not valid",
      })
    }
  }).catch(error => {
    res.status(500).send({
      status: false,
      message: "Password update failed !",
      error
    })
  })
}
